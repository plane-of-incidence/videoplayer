# videoplayer


* Raspberry pi 4
* POE+ hat
* USB Audio


## Specifications

RTC Synced video player

### Main Player

* VLC with HTTP interface


### Secondary Player

* FFmpeg sync to VLC

### LED 

* HyperHDR

```mermaid
graph TD;
     A-->B;
     A-->C;
     B-->D;
     C-->D;
```


Notes 

